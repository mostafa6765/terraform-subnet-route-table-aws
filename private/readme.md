# subnet_route_table module 
* create subnets.
* create route table.
* association route table and subnets.

```bash 
module "private_subnet_setup" {

  source = "./modules/subnet_route_table/private"
  vpc_id = ""

  // create private subnets
  subnets = [
    {cidr = "10.0.1.0/24", tag_name = "express_app_private_subnet"},
    {cidr = "10.0.2.0/24", tag_name = "database_private_subnet"},
  ]

  // create route table and association with private subnets.
  nat_gateway_id = ""
  project_name = ""
  table_tag_name = ""
  
}
```