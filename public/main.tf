locals {
  project_name = var.project_name
}

# create public subnet - 1
resource "aws_subnet" "this" {
  vpc_id = var.vpc_id
  cidr_block = var.subnet
  map_public_ip_on_launch = true
  tags = {
    "Name" = "${local.project_name}_${var.tag_name}"
  }
}


# create route table (public)
resource "aws_route_table" "this" {
  vpc_id = var.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.internet_gateway_id
  }
  tags = {
    "Name" = "${local.project_name}_${var.table_tag_name}"
  }
}

# public route table association
resource "aws_route_table_association" "this" {
  subnet_id = aws_subnet.this.id
  route_table_id = aws_route_table.this.id
}