output "subnet_id" {
  value = aws_subnet.this.id
}

output "subnet" {
  value = aws_subnet.this
}