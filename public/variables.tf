variable "vpc_id" {
  type = string
}

variable "project_name" {
  type = string
}

variable "subnet" {
  type = string
}

variable "tag_name" {
  type = string
}

variable "table_tag_name" {
  type = string
}

variable "internet_gateway_id" {
  type = string
}